SHELL = /bin/sh
export ROOTS_VERSION=master
export TERRAFORM_VERSION=0.12.5

export LOGO_THEME="bakery"

-include $(shell [ ! -d .roots ] && git clone --branch $(ROOTS_VERSION) https://gitlab.com/skalesys/roots.git .roots; echo .roots/Makefile)

